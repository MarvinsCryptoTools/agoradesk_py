# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.1] - 2022-05-02

### Fixed
- Issue #8 - Function on how to grab entire trade history (missing `page` method argument on dashboard* methods)

### Added
- More static code checks by adding [Bandit](https://pypi.org/project/bandit/),
[Safety](https://pypi.org/project/safety/), and [flake8-annotations](https://pypi.org/project/flake8-annotations/)

### Updated:
- Added missing type annotations
- Version of dependencies / requirements

### Removed:
- none

## [0.3.0] - 2022-02-25

### Added
- dlint and eradicate plugins to flake8
- Unit tests.
- Run unit test in Woodpecker CI
- Link to Changelog in poetry settings in pyproject.toml
- Missing API methods

### Updated:
- Versions of dependencies / requirements
- Minimum version of Python supported is now 3.8+ as mocked unit test fixture seems to be incompatible with earlier Python versions.
- Moved existing test to `tests\integration\` as the existing tests were really integration test.
- Changes needed for new version [1.7 of LocalMonero.co / Agoradesk API](https://agoradesk.com/api-docs/v1#2022-01-13-v17)

### Removed:
- Any commented out code

## [0.2.0] - 2022-01-09
- Updates needed to make implemented methods work with new version of AgoraDesk / LocalMonero API (v1.6)
- Also started using [Python Poetry](https://python-poetry.org/) for dependencies / requirements control.

## [0.1.0] - 2021-11-16

Initial release. Following API calls have been **not** implemented:
- /trade/contact_release/{trade_id} • Release trade escrow
- /contact_fund/{trade_id} • Fund a trade
- /contact_dispute/{trade_id} • Start a trade dispute
- /contact_escrow/{trade_id} • Enable escrow
- Image uploading in chat
- /contact_message_attachment/{trade_id}/{attachment_id} • Get a trade chat attachment
