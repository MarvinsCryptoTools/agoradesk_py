"""Contains pytest unit-tests for creating API object."""
# pylint: disable=missing-function-docstring
import arrow


__author__ = "marvin8"
__copyright__ = "(C) 2021 - 2022 https://codeberg.org/MarvinsCryptoTools/agoradesk_py"
__version__ = "0.3.0"


# Unit Test Trade API methods
# ======================================
def test_feedback_no_msg(mocked_api) -> None:
    response = mocked_api.feedback(
        username="Mock User",
        feedback="Mock Feedback",
        msg=None,
    )
    assert response["success"] is True


def test_feedback_with_msg(mocked_api) -> None:
    response = mocked_api.feedback(
        username="Mock User",
        feedback="Mock Feedback",
        msg="Mock Message",
    )
    assert response["success"] is True


def test_contact_release(mocked_api) -> None:
    response = mocked_api.contact_release(trade_id="Mock Trade")
    assert response["success"] is True


def test_contact_fund(mocked_api) -> None:
    response = mocked_api.contact_fund(trade_id="Mock Trade")
    assert response["success"] is True


def test_contact_dispute(mocked_api) -> None:
    response = mocked_api.contact_dispute(trade_id="Mock Trade")
    assert response["success"] is True


def test_contact_mark_as_paid(mocked_api) -> None:
    response = mocked_api.contact_mark_as_paid(trade_id="Mock Trade")
    assert response["success"] is True


def test_contact_cancel(mocked_api) -> None:
    response = mocked_api.contact_cancel(trade_id="Mock Trade")
    assert response["success"] is True


def test_contact_escrow(mocked_api) -> None:
    response = mocked_api.contact_escrow(trade_id="Mock Trade")
    assert response["success"] is True


def test_contact_messages_all(mocked_api) -> None:
    response = mocked_api.contact_messages(trade_id="Mock Trade ID")
    assert response["success"] is True


def test_contact_messages_after(mocked_api) -> None:
    response = mocked_api.contact_messages(trade_id="Mock Trade ID", after=arrow.now())
    assert response["success"] is True


def test_contact_create_no_msg_or_settlement_address(mocked_api) -> None:
    response = mocked_api.contact_create(
        ad_id="Mock Ad",
        amount=1234,
    )
    assert response["success"] is True


def test_contact_create_with_msg_and_settlement_details(mocked_api) -> None:
    response = mocked_api.contact_create(
        ad_id="Mock Ad",
        amount=1234,
        msg="Mock Msg",
        buyer_settlement_address="Mock Settlement Address",
        buyer_settlement_fee_level="Mock Settlement Fee Level",
    )
    assert response["success"] is True


def test_contact_info_string(mocked_api) -> None:
    response = mocked_api.contact_info(
        trade_ids="Mock Trade",
    )
    assert response["success"] is True


def test_contact_info_list(mocked_api) -> None:
    response = mocked_api.contact_info(
        trade_ids=["Mock Trade1", "Mock Trade2"],
    )
    assert response["success"] is True


def test_contact_message_post(mocked_api) -> None:
    response = mocked_api.contact_message_post(
        trade_id="Mock Trade",
        msg="Mock Message",
    )
    assert response["success"] is True


def test_contact_message_attachment(mocked_api) -> None:
    response = mocked_api.contact_message_attachment(
        trade_id="Mock Trade",
        attachment_id="Mock Attachment",
    )
    assert response["success"] is True
