"""Contains pytest unit-tests for creating API object."""
# pylint: disable=missing-function-docstring


__author__ = "marvin8"
__copyright__ = "(C) 2021 - 2022 https://codeberg.org/MarvinsCryptoTools/agoradesk_py"
__version__ = "0.3.0"


# Unit Test Statistics API methods
# ======================================
def test_moneroaverage(mocked_api) -> None:
    response = mocked_api.moneroaverage()
    assert response["success"] is True
