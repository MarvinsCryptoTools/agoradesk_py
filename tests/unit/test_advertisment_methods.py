"""Contains pytest unit-tests for creating API object."""
# pylint: disable=missing-function-docstring


__author__ = "marvin8"
__copyright__ = "(C) 2021 - 2022 https://codeberg.org/MarvinsCryptoTools/agoradesk_py"
__version__ = "0.3.0"


# Unit Test Advertisement API methods
# ======================================
def test_ad_create_max_cov(mocked_api) -> None:
    response = mocked_api.ad_create(
        country_code="Mock Country",
        currency="Mock Currency",
        trade_type="Mock Trade Type",
        asset="Mock Asset",
        price_equation="Mock Price Equation",
        track_max_amount=True,
        require_trusted_by_advertiser=True,
        verified_email_required=True,
        buyer_settlement_address="Mock Settlement Address",
        buyer_settlement_fee_level="Mock Settlement Fee Level",
        online_provider="Mock Online Provider",
        msg="Mock Message",
        min_amount=1.234,
        max_amount=5678,
        limit_to_fiat_amounts="Mock Fiat Amount",
        payment_method_details="Mock Payment Method Details",
        first_time_limit_asset=5.2,
        require_feedback_score=17,
        account_info="Mock Account Information",
        payment_window_minutes=37,
        floating=True,
        lat=1234.5678,
        lon=8765.4321,
    )
    assert response["success"] is True


def test_ad_max_cov(mocked_api) -> None:
    response = mocked_api.ad(
        ad_id="Mock Ad",
        country_code="Mock Country",
        currency="Mock Currency",
        trade_type="Mock Trade Type",
        asset="Mock Asset",
        price_equation="Mock Price Equation",
        track_max_amount=True,
        require_trusted_by_advertiser=True,
        verified_email_required=True,
        buyer_settlement_address="Mock Settlement Address",
        buyer_settlement_fee_level="Mock Settlement Fee Level",
        online_provider="Mock Online Provider",
        msg="Mock Message",
        min_amount=1.234,
        max_amount=567.8,
        limit_to_fiat_amounts="Mock Limit",
        payment_method_details="Mock Payment Method",
        first_time_limit_asset=37.89,
        require_feedback_score=17,
        account_info="Mock Account Information",
        payment_window_minutes=47,
        floating=True,
        lat=1234.5678,
        lon=8765.4321,
        visible=True,
    )
    assert response["success"] is True


def test_ad_equation(mocked_api) -> None:
    response = mocked_api.ad_equation(
        ad_id="Mock Ad",
        price_equation="Mock Price Equation",
    )
    assert response["success"] is True


def test_ad_delete(mocked_api) -> None:
    response = mocked_api.ad_delete(ad_id="Mock Ad")
    assert response["success"] is True


def test_ads_visible(mocked_api) -> None:
    response = mocked_api.ads(
        country_code="Mock Country Code",
        currency="Mock Currency",
        trade_type="Mock Trade Type",
        visible=True,
        asset="Mock Asset",
        payment_method_code="Mock Payment Method",
    )
    assert response["success"] is True


def test_ads_not_visible(mocked_api) -> None:
    response = mocked_api.ads(visible=False)
    assert response["success"] is True


def test_ads_no_params(mocked_api) -> None:
    response = mocked_api.ads()
    assert response["success"] is True


def test_ad_get_one_ad(mocked_api) -> None:
    response = mocked_api.ad_get(ad_ids=["Mock Ad1"])
    assert response["success"] is True


def test_ad_get_three_ad(mocked_api) -> None:
    response = mocked_api.ad_get(ad_ids=["Mock Ad1", "Mock Ad2", "Mock Ad3"])
    assert response["success"] is True


def test_payment_methods(mocked_api) -> None:
    response = mocked_api.payment_methods(country_code="Mock Country Code")
    assert response["success"] is True


def test_country_codes(mocked_api) -> None:
    response = mocked_api.country_codes()
    assert response["success"] is True


def test_currency_codes(mocked_api) -> None:
    response = mocked_api.currencies()
    assert response["success"] is True


def test_equation(mocked_api) -> None:
    response = mocked_api.equation(
        price_equation="Mock Price Equation",
        currency="Mock Currency",
    )
    assert response["success"] is True
