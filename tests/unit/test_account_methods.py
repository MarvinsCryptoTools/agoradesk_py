"""Contains pytest unit-tests for creating API object."""
# pylint: disable=missing-function-docstring


__author__ = "marvin8"
__copyright__ = "(C) 2021 - 2022 https://codeberg.org/MarvinsCryptoTools/agoradesk_py"
__version__ = "0.3.0"


# Unit Test Account API methods
# ======================================
def test_account_info(mocked_api) -> None:
    response = mocked_api.account_info(username="Mock User")
    assert response["success"] is True


def test_dashboard(mocked_api) -> None:
    response = mocked_api.dashboard()
    assert response["success"] is True


def test_dashboard_buyer(mocked_api) -> None:
    response = mocked_api.dashboard_buyer()
    assert response["success"] is True


def test_dashboard_seller(mocked_api) -> None:
    response = mocked_api.dashboard_seller()
    assert response["success"] is True


def test_dashboard_canceled(mocked_api) -> None:
    response = mocked_api.dashboard_canceled()
    assert response["success"] is True


def test_dashboard_closed(mocked_api) -> None:
    response = mocked_api.dashboard_closed()
    assert response["success"] is True


def test_dashboard_released(mocked_api) -> None:
    response = mocked_api.dashboard_released()
    assert response["success"] is True


def test_logout(mocked_api) -> None:
    response = mocked_api.logout()
    assert response["success"] is True


def test_myself(mocked_api) -> None:
    response = mocked_api.myself()
    assert response["success"] is True


def test_notifications(mocked_api) -> None:
    response = mocked_api.notifications()
    assert response["success"] is True


def test_notifications_mark_as_read(mocked_api) -> None:
    response = mocked_api.notifications_mark_as_read(
        notification_id="Mock Notification"
    )
    assert response["success"] is True


def test_recent_messages(mocked_api) -> None:
    response = mocked_api.recent_messages()
    assert response["success"] is True
