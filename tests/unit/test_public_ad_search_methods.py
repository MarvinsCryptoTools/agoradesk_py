"""Contains pytest unit-tests for creating API object."""
# pylint: disable=missing-function-docstring


__author__ = "marvin8"
__copyright__ = "(C) 2021 - 2022 https://codeberg.org/MarvinsCryptoTools/agoradesk_py"
__version__ = "0.3.0"


# Unit Test Public Ad Search API methods
# ======================================
def test_buy_monero_online_all_params(mocked_api) -> None:
    response = mocked_api.buy_monero_online(
        currency_code="Mock Currency",
        country_code="Mock Country",
        payment_method="Mock Payment Method",
        amount=12.34,
        page=3,
    )
    assert response["success"] is True


def test_sell_monero_online_only_amount_not_page(mocked_api) -> None:
    response = mocked_api.sell_monero_online(
        currency_code="Mock Currency",
        country_code="Mock Country",
        payment_method="Mock Payment Method",
        amount=12.34,
    )
    assert response["success"] is True


def test_buy_bitcoins_online_only_page_not_amount(mocked_api) -> None:
    response = mocked_api.buy_bitcoins_online(
        currency_code="Mock Currency",
        country_code="Mock Country",
        payment_method="Mock Payment Method",
        page=3,
    )
    assert response["success"] is True


def test_sell_bitcoins_online_no_amount_no_page(mocked_api) -> None:
    response = mocked_api.sell_bitcoins_online(
        currency_code="Mock Currency",
        country_code="Mock Country",
        payment_method="Mock Payment Method",
    )
    assert response["success"] is True


def test_buy_monero_with_cash(mocked_api) -> None:
    response = mocked_api.buy_monero_with_cash(
        currency_code="Mock Currency",
        country_code="Mock Country",
        lat=12.34,
        lon=56.78,
    )
    assert response["success"] is True


def test_sell_monero_with_cash(mocked_api) -> None:
    response = mocked_api.sell_monero_with_cash(
        currency_code="Mock Currency",
        country_code="Mock Country",
        lat=12.34,
        lon=56.78,
    )
    assert response["success"] is True


def test_buy_bitcoins_with_cash(mocked_api) -> None:
    response = mocked_api.buy_bitcoins_with_cash(
        currency_code="Mock Currency",
        country_code="Mock Country",
        lat=12.34,
        lon=56.78,
    )
    assert response["success"] is True


def test_sell_bitcoins_with_cash(mocked_api) -> None:
    response = mocked_api.sell_bitcoins_with_cash(
        currency_code="Mock Currency",
        country_code="Mock Country",
        lat=12.34,
        lon=56.78,
    )
    assert response["success"] is True
