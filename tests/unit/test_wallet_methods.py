"""Contains pytest unit-tests for creating API object."""
# pylint: disable=missing-function-docstring


__author__ = "marvin8"
__copyright__ = "(C) 2021 - 2022 https://codeberg.org/MarvinsCryptoTools/agoradesk_py"
__version__ = "0.3.0"


# Unit Test Wallet API methods
# ======================================
def test_wallet(mocked_api) -> None:
    response = mocked_api.wallet()
    assert response["success"] is True


def test_wallet_balance(mocked_api) -> None:
    response = mocked_api.wallet_balance()
    assert response["success"] is True


def test_wallet_xmr(mocked_api) -> None:
    response = mocked_api.wallet_xmr()
    assert response["success"] is True


def test_wallet_balance_xmr(mocked_api) -> None:
    response = mocked_api.wallet_balance_xmr()
    assert response["success"] is True


def test_wallet_addr(mocked_api) -> None:
    response = mocked_api.wallet_addr()
    assert response["success"] is True


def test_wallet_addr_xmr(mocked_api) -> None:
    response = mocked_api.wallet_addr_xmr()
    assert response["success"] is True


def test_fees_no_address(mocked_api) -> None:
    response = mocked_api.fees()
    assert response["success"] is True


def test_fees_and_address(mocked_api) -> None:
    response = mocked_api.fees(address="Mock Address")
    assert response["success"] is True


def test_fees_xmr(mocked_api) -> None:
    response = mocked_api.fees_xmr()
    assert response["success"] is True


def test_wallet_send(mocked_api) -> None:
    response = mocked_api.wallet_send(
        address="Mock address",
        amount=12.34,
        password="Mock password",
        fee_level=13,
        otp="Mock OTP",
    )
    assert response["success"] is True


def test_wallet_send_xmr(mocked_api) -> None:
    response = mocked_api.wallet_send_xmr(
        address="Mock address",
        amount=12.34,
        password="Mock password",
        fee_level=13,
        otp="Mock OTP",
    )
    assert response["success"] is True
