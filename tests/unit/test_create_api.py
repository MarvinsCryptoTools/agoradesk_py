"""Contains pytest unit-tests for creating API object."""
# pylint: disable=missing-function-docstring
from agoradesk_py.agoradesk import AgoraDesk

__author__ = "marvin8"
__copyright__ = "(C) 2021 - 2022 https://codeberg.org/MarvinsCryptoTools/agoradesk_py"
__version__ = "0.3.0"


# Unit Test API Object initialisation
# ======================================
def test_empty_api_key() -> None:
    local_monero = AgoraDesk(api_key="")
    assert isinstance(local_monero, AgoraDesk)


def test_debug_true() -> None:
    local_monero = AgoraDesk(api_key="Unit Test API Key", debug=True)
    assert isinstance(local_monero, AgoraDesk)
