"""Containing the PyTest fixtures."""
# pylint: disable=duplicate-code
from typing import Any
from typing import Dict
from typing import Optional

import pytest

from agoradesk_py.agoradesk import AgoraDesk

__author__ = "marvin8"
__copyright__ = "(C) 2021 - 2022 https://codeberg.org/MarvinsCryptoTools/agoradesk_py"
__version__ = "0.3.0"


@pytest.fixture(scope="session")
def mocked_api(session_mocker):
    """Create a Mock API that is not actually calling LocalMonero /
    Agoradesk."""

    def mock_api_call(
        self,
        api_method: str,
        http_method: Optional[str] = "GET",
        query_values: Optional[Dict[str, Any]] = None,
    ) -> Dict[str, Any]:
        print(f"{api_method=}")
        return {
            "success": True,
            "message": "Mock Response",
            "response": None,
            "status": None,
        }

    session_mocker.patch("agoradesk_py.agoradesk.AgoraDesk._api_call", mock_api_call)

    yield AgoraDesk(api_key="Mock Key", debug=True)
